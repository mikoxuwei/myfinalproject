<%-- 
    Document   : idpwError
    Created on : 2022年12月29日, 上午1:14:54
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>錯誤！請確認您是否擁有帳號，或者密碼是否正確。</h1><br/>
        <a href = "signup.jsp"> 前往註冊 </a>
        <a style='margin-left: 3rem;' href = "index.jsp"> 返回登入 </a><br/>
    </body>
</html>
