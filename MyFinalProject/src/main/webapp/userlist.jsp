<%-- 
    Document   : userlist
    Created on : 2022年12月28日, 下午4:50:56
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User List</title>
        <style>
            .table-container {
                width: 70%;
            }
            
            th, td {
                width: 12%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <a style='margin-left: 1rem;' href = 'logout'>登出</a>
        <a style='margin-left: 3rem;' href = 'characterlist.jsp'>全遊戲角色</a>
        <div>
            <table class="table-container">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Id</th>
                        <th>Password</th>
                        <th>Email</th>
                        <!--<th>Num</th>-->
                    </tr>
                </thead>
                <tbody id="list">
                    <%
                        UserDao userDao = new UserDao();
                        List<User> users = userDao.findAll();
                        int n = 0;
                        for (User item : users) {
                    %>
                                <tr>
                                    <td><%= item.getName() %></td>
                                   <td><%= item.getId() %></td>
                                   <td><%= item.getPassword()%></td>
                                   <td><%= item.getEmail()%></td>
                                   <!--<td><%= item.getNum()%></td>-->
                                   <td><a href = 'update.jsp?id=<%= item.getId() %>&password=<%= item.getPassword() %>'>修改密碼</a></td>
                                   <td><a href = 'deleteUser?id=<%= item.getId() %>&num=<%= item.getNum()%>'>刪除</a></td>
                                   <!--<td><a href = 'characterlist.jsp?id=<%= item.getId()%>'>遊戲角色</a></td>-->
                                   <td><a href = 'myCharacter?num=<%= item.getNum()%>'>遊戲角色</a></td>
                                </tr>
                                
                    <%                                    
                        }
                    %>
                </tbody>
            </table>
        </div>
    </body>
</html>
