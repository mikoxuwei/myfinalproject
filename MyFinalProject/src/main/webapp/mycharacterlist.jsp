<%-- 
    Document   : mycharacterlistjsp
    Created on : 2022年12月28日, 下午11:16:44
    Author     : miko
--%>

<%@page import="java.util.List"%>
<%@page import="characterpackage.CharacterDao"%>
<%@page import="java.util.List"%>
<%@page import="characterpackage.Character"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Character List</title>
        <style>
            .table-container {
                width: 70%;
            }
            
            th, td {
                width: 15%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <a style='margin-left: 1rem;' href = "addchar.jsp"> 創建角色 </a>
        <a style='margin-left: 3rem;' href = 'logout'>登出</a>
        <%
            if (session.getAttribute("role").equals("admin")) {
        %>
                <a style='margin-left: 3rem;' href = 'userlist.jsp'>使用者清單</a>
        <%    
            }
        %>
        <div>
            <table class="table-container">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Sex</th>
                        <th>Race</th>
                        <th>Rarity</th>
                        <th>Level</th>
                        <th>Play</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody id="list">
                    <%
                        CharacterDao characterDao = new CharacterDao();
                        List<Character> characters = characterDao.findByNum(characterDao.takenum());
                        int n = 0;
                        for (Character item : characters) {
                            n = n++;
                    %>
                                <tr>
                                   <td><%= item.getName() %></td>
                                   <td><%= item.getSex() %></td>
                                   <td><%= item.getRace() %></td>
                                   <td><%= item.getRarity() %></td>
                                   <td><%= item.getLevel() %></td>
                                   <td><a href = 'levelup?id=<%= item.getId() %>&level=<%= item.getLevel()%>'>Play</a></td>
                                   <td><a href = 'deleteCharacter?id=<%= item.getId() %>'>Delete</a></td>  
                                </tr>
                                
                    <%                                    
                        }
                    %>
                </tbody>
            </table>
        </div>
    </body>
</html>
