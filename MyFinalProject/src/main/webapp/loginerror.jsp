<%-- 
    Document   : loginerror
    Created on : 2022年12月30日, 上午12:31:43
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Error</title>
    </head>
    <body>
        <h1>請填入帳號密碼，或是前往註冊！</h1>
        <a href = "index.jsp"> 返回登入 </a>
        <a style ="margin-left: 3rem" href = "signup.jsp"> 前往註冊 </a><br/>
    </body>
</html>
