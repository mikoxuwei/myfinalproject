<%-- 
    Document   : login
    Created on : 2022年11月30日, 下午5:00:42
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>登入</title>
        <style>
            .input {padding: 3px 9px;
            -webkit-border-radius: 5px;
            border-radius: 5px; 
            }
        </style>
    </head>
    <body>
        <a href = "signup.jsp"> 註冊 </a><br/>
        <form method = "post" action = "login">
            帳號： <input type="text" name = "id"/><br/>
            密碼： <input name = "password" type = "password"/><br/>
            <input type = "submit" value = "登入"/>
        </form>
    </body>
</html>
