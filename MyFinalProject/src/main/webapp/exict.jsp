<%-- 
    Document   : exict
    Created on : 2022年12月29日, 下午11:05:50
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>帳號已使用</title>
    </head>
    <body>
        <h1>錯誤！此帳號已有人使用，請前往登入，或是選擇其他帳號組合。</h1>
        <a href = "signup.jsp"> 返回註冊 </a>
        <a style ="margin-left: 3rem" href = "index.jsp"> 前往登入 </a><br/>
    </body>
</html>
