<%-- 
    Document   : signup
    Created on : 2022年12月28日, 下午4:12:24
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>註冊</title>
        <style>
            .input {padding: 3px 9px;
            -webkit-border-radius: 5px;
            border-radius: 5px; 
            }
        </style>
    </head>
    <body>
        <a href = "clearServlet"> 清除 </a>
        <a style='margin-left: 3rem;' href = "index.jsp"> 登入 </a><br/>
        <form method = "post" action = "addUserServlet">
            暱稱： <input type="text" name = "name"/><br/>
            帳號： <input type="text" name = "id"/><br/>
            密碼： <input name = "password" type = "password"/><br/>
            信箱： <input name = "email" type = "email"/><br/>
            <input type = "submit" value = "註冊"/>
        </form>
    </body>
</html>
