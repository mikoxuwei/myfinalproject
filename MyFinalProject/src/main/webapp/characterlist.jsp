<%-- 
    Document   : characterlist
    Created on : 2022年12月7日, 下午11:52:30
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="characterpackage.CharacterDao"%>
<%@page import="java.util.List"%>
<%@page import="characterpackage.Character"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Character List</title>
        <style>
            .table-container {
                width: 70%;
            }
            
            th, td {
                width: 15%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <a style='margin-left: 1rem;' href = "userlist.jsp"> 返回 </a>
        <a style='margin-left: 3rem;' href = "logout"> 登出 </a>
        <div>
            <table class="table-container">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>CharacterName</th>
                        <th>Sex</th>
                        <th>Race</th>
                        <th>Rarity</th>
                        <th>Level</th>                       
                    </tr>
                </thead>
                <tbody id="list">
                    <%
                        
                        CharacterDao characterDao = new CharacterDao();
                        List<Character> characters = characterDao.findAll();
                        UserDao userDao = new UserDao();
                        int n = 0;
                        for (Character item : characters) {
                            n = n++;
                    %>
                                <tr>
                                   <td><%= userDao.findByNum(item.getNum()) %></td>
                                   <td><%= item.getName() %></td>
                                   <td><%= item.getSex() %></td>
                                   <td><%= item.getRace() %></td>
                                   <td><%= item.getRarity() %></td>
                                   <td><%= item.getLevel() %></td>                      
                                   <td><a href = 'levelupAll?id=<%= item.getId() %>&level=<%= item.getLevel()%>'>Play</a></td>
                                   <td><a href = 'deleteCharacter?id=<%= item.getId() %>'>Delete</a></td>  
                                </tr>
                                
                    <%                                    
                        }
                    %>
                </tbody>
            </table>
        </div>
                    
    </body>
</html>
