<%-- 
    Document   : update
    Created on : 2022年12月28日, 下午8:59:49
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>修改密碼</title>
    </head>
    <body>
        <%
            UserDao dao = new UserDao();
            User user = dao.findById(request.getParameter("id"));
        %>
        <a href = "userlist.jsp"> 返回 </a><br/>
        <form method = "post" action = "update">
            帳號： <input type="text" name = "id" value="<%= user.getId()%>"/><br/>
            密碼： <input name = "password" type = "password" value="<%= user.getPassword()%>"/><br/>
            <input type = "submit" value = "修改"/>
        </form>
    </body>
</html>
