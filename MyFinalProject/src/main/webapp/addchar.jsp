<%@page import="characterpackage.CharacterDao"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="characterpackage.Character"%>
<%@page contentType = "text/html; charset=UTF-8" language = "Java" %>
<!DOCTYPE html>
<html>
    <head>
        <title>創建角色</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
/*            .line{
            display:inline;
            }*/
            .input {padding: 3px 9px;
            -webkit-border-radius: 5px;
            border-radius: 5px; 
            }
            label {
               cursor: pointer; 
            }
            .p{
                cursor: pointer;
                padding: 5px 15px;
                -webkit-border-radius: 5px;
                border-radius: 5px;
                margin-top: 1rem;
            }
            div{
                margin-top: 0.25rem;
            }
            .select {padding: 3px 9px;
            -webkit-border-radius: 5px;
            border-radius: 5px; 
            }
            
        </style>
    </head>
    
    <body>
        
        <a href = "clearCharacter"> 清除 </a>
        <!--<a href = "loginServlet"> 登入 </a><br/>-->
        <a style='margin-left: 3rem;' href = "mycharacterlist.jsp"> 角色清單 </a><br/>
        <form method = "post" action = "addCharacterServlet">
                <div>
                    名字：<input name = "name" value = ""/>
                </div>
                <div>
                    性別：
                    <label><input type="radio" name="sex" value="male"> 男</label>
                    <label><input type="radio" name="sex" value="female"> 女</label>
                    <label><input type="radio" name="sex" value="else"> 其他</label>
                </div>
                <div>
                    種族：
                    <select class="input" name="race">
                        <!-- <optgroup label="攻擊"> -->
                        <option value="human"> 人族</option>
                        <option value="cat"> 獸人族</option>
                        <!-- </optgroup>
                        <optgroup label="輔助">  -->
                        <option value="dwarf"> 矮人族</option>
                        <option value="elf"> 精靈族</option>
                         <!-- </optgroup> -->
                    </select>
                </div>
                <div>
                    稀有度：未知 
                </div>
                <div>
                    等級：1
                </div>
            <input class="p" type = "submit" value = "創建"/>
        </form>
    </body>
</html>

