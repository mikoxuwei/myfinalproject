/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package characterpackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.MapKeyEnumerated;

/**
 *
 * @author miko
 */
public class CharacterDao {
    static int mynum;
    
    static List<String> rarityList = new ArrayList<String>(){{
        add("SSR");
        add("SR");
        add("R");
        add("N");
    }};
    public int getnum(int num){
        mynum = num;
        return 0;
    }
    public int takenum(){
        return mynum;
    }
    public void addCharacter(Character character) {
        try ( Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "");) {
                                    
//            List<String> rarityList = new ArrayList<String>()
//            rarityList.add("SSR");
//            rarityList.add("SR");
//            rarityList.add("R");
//            rarityList.add("N");
            Random random = new Random();

            int id = (int) ((Math.random() *9+1)*10000);
            
            int randomInt = random.nextInt(100);
            int raritynum = 3;
            if (randomInt == 99){
                raritynum = 0;
            }else if(randomInt < 98 && randomInt >= 78){
                raritynum = 1;
            }else if(randomInt < 78 && randomInt >= 47){
                raritynum = 2;
            }else{
                raritynum = 3;
            }

            String name = character.getName();

            String sex = null;
            if (character.getSex().equals("male")){
                sex = "男";
            }else if(character.getSex().equals("female")){
                sex = "女";
            }else{
                sex = "其他";
            }
            String race = null;
            if (character.getRace().equals("human")){
                race = "人族";
            }else if(character.getRace().equals("cat")){
                race = "獸人族";
            }else if(character.getRace().equals("dwarf")){
                race = "矮人族";
            }else if(character.getRace().equals("elf")){
                race = "精靈族";
            }
            
            String rarity = rarityList.get(raritynum);
            int level = 1;
            int num = mynum;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("insert into mycharacter (id, name, sex, race, rarity, level, num) "
                    + "values ('" + id + "' , '" + name + "' , '" + sex + "' ,'" + race + "' , '" + rarity + "' , '" + level + "', '" + num + "')");
        } catch (SQLException ex) {
            Logger.getLogger(AddCharacterServlet.class.getName()).log(
                    Level.SEVERE, null, ex); // 錯誤訊息，但是只有開發人員看的到。
        }
    }
    public void deleteCharacter(Character character) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            int id = character.getId();
            stmt.executeUpdate("delete from mycharacter where id='"+ id +"'");
        }catch (SQLException ex) {
            Logger.getLogger(CharacterListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    public void deleteCharacterFromUser(Character character) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            int num = character.getNum();
//            ResultSet rs = stmt.executeQuery("select id from mycharacter where num='"+ num +"'");
//            while (rs.next()) {
//                int id = character.getId();
//                stmt.executeUpdate("delete from mycharacter where id='"+ id +"'");
//            }
            stmt.executeUpdate("delete from mycharacter where num='"+ num +"'");
        }catch (SQLException ex) {
            Logger.getLogger(CharacterListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    public void levelup(Character character) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            int id = character.getId();
            int level = character.getLevel();
            level = level + 1;
            stmt.executeUpdate("UPDATE mycharacter SET level = '" 
                    + level +"' WHERE id = '"+ id +"'");
        }catch (SQLException ex) {
            Logger.getLogger(CharacterListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    public List<Character> findAll(){
        List<Character> ret = new ArrayList<Character>();
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from mycharacter");
            while (rs.next()) {
                Character character = new Character();
                character.setId(rs.getInt("id"));
                character.setName(rs.getString("name"));
                character.setSex(rs.getString("sex"));
                character.setRace(rs.getString("race"));
                character.setRarity(rs.getString("rarity"));
                character.setLevel(rs.getInt("level"));
                character.setNum(rs.getInt("num"));
//                System.out.println(character.getName());
                ret.add(character);
            }
        }catch (SQLException ex) {
            Logger.getLogger(CharacterListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    return ret;
    }
    public Character findById(int id){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select * from mycharacter where id='" +id+"'");
             if(rs.next()){
                Character character = new Character();
                character.setId(rs.getInt("id"));
                character.setLevel(rs.getInt("level"));
                return character;
            }
        }catch (SQLException ex) {
                Logger.getLogger(CharacterListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Character> findByNum(int num){
        List<Character> ret = new ArrayList<Character>();
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select * from mycharacter where num='" +num+"'");
             while(rs.next()){
                Character character = new Character();
                character.setId(rs.getInt("id"));
                character.setName(rs.getString("name"));
                character.setSex(rs.getString("sex"));
                character.setRace(rs.getString("race"));
                character.setRarity(rs.getString("rarity"));
                character.setLevel(rs.getInt("level"));
                ret.add(character);
            }
        }catch (SQLException ex) {
                Logger.getLogger(CharacterListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return ret;
    }
}
